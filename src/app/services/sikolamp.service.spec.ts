import { TestBed } from '@angular/core/testing';

import { SikolampService } from './sikolamp.service';

describe('SikolampService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SikolampService = TestBed.get(SikolampService);
    expect(service).toBeTruthy();
  });
});
