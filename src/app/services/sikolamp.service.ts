import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user.interface';
import { Lampu } from '../models/lampu.interface';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SikolampService {

  constructor(
    public router: Router,
    public toastController: ToastController
  ) { }
  async message(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
  toast.present();
  }
}
