import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDrthAetXFznwthYGfGsEhuKjuFhIqQnF8",
  authDomain: "sikolamp-fd2e2.firebaseapp.com",
  databaseURL: "https://sikolamp-fd2e2.firebaseio.com",
  projectId: "sikolamp-fd2e2",
  storageBucket: "sikolamp-fd2e2.appspot.com",
  messagingSenderId: "1021687733158"
};

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {this.initializeApp();}

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}
