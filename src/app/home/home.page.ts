import { Component } from '@angular/core';
import { SikolampService } from '../services/sikolamp.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { AdminPage } from '../pages/admin/admin.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  users = [];
  data = {
    username: '',
    password: ''
  };

  constructor(
    public router: Router,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public sikolampService: SikolampService,
    public modalController: ModalController
  ){
    firebase.database().ref('t_user').on('value', resp => {
      this.users = [];
      this.users = snapshotToArray(resp);
    });
  }

  login(){
    console.log(this.users);
    for(let data of this.users){
      if (this.data.username == data.username){
        if(this.data.password == data.password){
          if(data.hak_akses == "0"){
            this.router.navigate(['/admin']);
            this.data = {
              username: '',
              password: ''
            };
          } else {
            this.router.navigate(['/pelanggan/'+data.key])
            this.data = {
              username: '',
              password: ''
            };
          }
        } else {
          this.sikolampService.message('Username atau Password Salah !');
          this.data = {
            username: '',
            password: ''
          };
        }
      } 
    }
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
}