export interface Lampu {
    id_lampu: number;
    username: string;
    status_lampu: number;
    status_fitur: number;
    keterangan: string;
}