export interface User {
    username: string;
    password: string;
    hak_akses: number;
}