import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import { snapshotToArray } from '../admin/admin.page';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage {
  data = {
    key: '',
    username: '',
    password: '',
    hak_akses: ''
  };
  lampu = [];
  fitur = [];
  key = this.route.snapshot.paramMap.get('key')

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public alertController: AlertController,
  ){
    firebase.database().ref('t_user/' + this.key).on('value', resp => {
      this.data = snapshotToObject(resp);
      console.log(this.data);
    });

    firebase.database().ref('t_lampu/'+ this.data.username).on('value', resp => {
      this.lampu = [];
      this.lampu = snapshotToArray(resp);
    });

    firebase.database().ref('t_fitur/'+ this.data.username).on('value', resp => {
      this.fitur = [];
      this.fitur = snapshotToArray(resp);
    });
  }

  async delete(key){
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Are you sure want to delete this info ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('cancel');
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref('t_lampu/'+this.data.username+'/'+key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}