import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { SikolampService } from 'src/app/services/sikolamp.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-add-lampu',
  templateUrl: './add-lampu.page.html',
  styleUrls: ['./add-lampu.page.scss'],
})
export class AddLampuPage implements OnInit {
  data = {
    username: '',
    password: '',
    hak_akses: ''
  };
  dataForm = {
    username : '',
    Keterangan: '',
  };
  public create_Lampu: FormGroup;
  public create_Fitur: FormGroup;
  ref = firebase.database().ref('t_lampu/');
  key = this.route.snapshot.paramMap.get('key')

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sikolampService: SikolampService,
    private formBuilder: FormBuilder,
    public router: Router,
    public route: ActivatedRoute
  ) {
    firebase.database().ref('t_user/' + this.key).on('value', resp => {
      this.data = snapshotToObject(resp);
      console.log(this.data);
    });
    console.log(this.key);

    this.dataForm = {
      username : this.data.username,
      Keterangan: '',
    };
    this.create_Lampu = this.formBuilder.group({
      s_lampu: ["OFF"]
      // username: [this.data.username],
    });
    console.log(this.create_Lampu)
    this.create_Fitur = this.formBuilder.group({
      s_fitur: ["OFF"]
      // username: [this.data.username],
    });
    console.log(this.create_Fitur)
  }

  createLampu() {
    firebase.database().ref('t_lampu/'+this.data.username+'/'+this.dataForm.Keterangan+'/lampu').set(this.create_Lampu.value);
    this.router.navigate(['/admin']);
    firebase.database().ref('t_lampu/'+this.data.username+'/'+this.dataForm.Keterangan+'/fitur').set(this.create_Fitur.value);
  }

  ngOnInit() {
  }
}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}