import { Component, OnInit } from '@angular/core';
import { SikolampService } from 'src/app/services/sikolamp.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage{
  UserList = [];
  ref = firebase.database().ref('t_user/');

  constructor(
    public router: Router, 
    public loadingController: LoadingController, 
    public alertController: AlertController,
    public modalController: ModalController,
    public sikolampService: SikolampService
  ){
    this.ref.on('value', resp => {
      this.UserList = [];
      this.UserList = snapshotToArray(resp);
    })
  }

  logout(){
    console.log("Anda Melakukan Logout");
    this.router.navigate(['/home']);
  }

  detailUser(key){
    this.router.navigate(['/pages/user-detail/'+key]);
  }

  async delete(key, name){
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Are you sure want to delete this info ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('cancel');
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref('t_user/'+key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
}