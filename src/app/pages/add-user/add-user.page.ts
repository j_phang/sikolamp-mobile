import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { SikolampService } from 'src/app/services/sikolamp.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {

  public createForm: FormGroup;
  ref = firebase.database().ref('t_user/');

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sikolampService: SikolampService,
    private formBuilder: FormBuilder,
    public router: Router
  ) {
    this.createForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      hak_akses: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  createUser() {
    let newInfo = firebase.database().ref('t_user/').push();
    newInfo.set(this.createForm.value);
    this.router.navigate(['/admin']);
  }
}
