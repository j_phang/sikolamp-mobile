import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { snapshotToArray } from '../admin/admin.page';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: ['./pelanggan.page.scss'],
})
export class PelangganPage {

  infoForm: FormGroup;
  fitur = [];
  lampu = [];
  user = {
    username: '',
    password: '',
    hak_akses: ''
  };
  key = this.route.snapshot.paramMap.get('key')

  constructor(
    public route : ActivatedRoute,
    public router: Router,
    public formBuilder: FormBuilder,
    public alertControlller: AlertController
  )
  {
    firebase.database().ref('t_user/' + this.key).on('value', resp => {
      this.user = snapshotToObject(resp);
      console.log(this.user)
    });

    firebase.database().ref('t_lampu/').child(this.user.username).on('value', resp => {
      this.lampu = [];
      this.lampu = snapshotToArray(resp);
      console.log(this.lampu);
    });
  }

  logout(){
    console.log("Anda Melakukan Logout");
    this.router.navigate(['/home']);
  }

   async updateLampu(key) {
    let alert = await this.alertControlller.create({
      header: 'Prompt!',
      inputs: [
        {
          name: 'status_lampu',
          placeholder: '1/0'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Update',
          handler:data => {
            if(data.status_lampu!== undefined && data.status_lampu!== null){
              firebase.database().ref('t_lampu/'+this.user.username+'/'+key+'/lampu').update({s_lampu:data.status_lampu})
            }
            console.log('Confirm Ok');
          }
        }
      ]
    });
   await alert.present();
  }
  
  async updateFitur(key) {
    let alert = await this.alertControlller.create({
      header: 'Prompt!',
      inputs: [
        {
          name: 'status_fitur',
          placeholder: '1/0'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Update',
          handler:data => {
            if(data.status_fitur!== undefined && data.status_fitur!== null){
              firebase.database().ref('t_lampu/'+this.user.username+'/'+key+'/fitur').update({s_fitur:data.status_fitur})
            }
            console.log('Confirm Ok');
          }
        }
      ]
    });

   await alert.present();
  }
 }

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}
