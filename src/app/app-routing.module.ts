import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'add-lampu', loadChildren: './pages/add-lampu/add-lampu.module#AddLampuPageModule' },
  { path: 'add-user', loadChildren: './pages/add-user/add-user.module#AddUserPageModule' },
  { path: 'admin', loadChildren: './pages/admin/admin.module#AdminPageModule' },
  { path: 'pelanggan', loadChildren: './pages/pelanggan/pelanggan.module#PelangganPageModule' },
  { path: 'pelanggan/:key', loadChildren: './pages/pelanggan/pelanggan.module#PelangganPageModule' },
  { path: 'user-detail', loadChildren: './pages/user-detail/user-detail.module#UserDetailPageModule' },
  { path: 'user-detail/:key', loadChildren: './pages/user-detail/user-detail.module#UserDetailPageModule' },
  { path: 'add-lampu/:key', loadChildren: './pages/add-lampu/add-lampu.module#AddLampuPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
